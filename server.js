const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

//Initializing server
const app = express();
app.use(bodyParser.json());
const port = 8080;
app.listen(port, () => {
    console.log("Server online on: " + port);
});

app.use('/' , express.static('../front-end'))

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "siscit_abonament"
});

connection.connect(function (err) {

    console.log("Connected to database!");
    const sql = "CREATE TABLE IF NOT EXISTS abonament(nume VARCHAR(40),prenume VARCHAR(40),telefon VARCHAR(15),email VARCHAR(50) UNIQUE,facebook VARCHAR(60),tipAbonament VARCHAR(20),nrCard VARCHAR(40),cvv INTEGER, varsta VARCHAR(3), cnp VARCHAR(20), sex VARCHAR(1), dataNasterii VARCHAR(10))";
    connection.query(sql, function (err, result) {
        if (err) throw err;
    });
});

app.post("/abonament", (req, res) => {
    let nume = req.body.nume;
    let prenume = req.body.prenume;
    let telefon = req.body.telefon;
    let email = req.body.email;
    let facebook = req.body.facebook;
    let tipAbonament = req.body.tipAbonament;
    let nrCard = req.body.nrCard;
    let cvv = req.body.cvv;
    let varsta = req.body.varsta;
    let cnp = req.body.cnp;
    let sex = req.body.sex;
    let dataNasterii = req.body.dataNasterii;
    let error = [];

    if (!nume||!prenume||!telefon||!email||!facebook||!tipAbonament||!nrCard||!cvv || !varsta || !cnp || !dataNasterii) {
        error.push("Unul sau mai multe campuri nu au fost introduse");
        console.log("Unul sau mai multe campuri nu au fost introduse!");
      } else {
        if (nume.length < 2 || nume.length > 20) {
          console.log("Nume invalid!");
          error.push("Nume invalid");
        } else if (!nume.match("^[A-Za-z]+$")) {
          console.log("Numele trebuie sa contina doar litere!");
          error.push("Numele trebuie sa contina doar litere!");
        }
        if (prenume.length < 2 || prenume.length > 20) {
          console.log("Prenume invalid!");
          error.push("Prenume invalid!");
        } else if (!prenume.match("^[A-Za-z]+$")) {
          console.log("Prenumele trebuie sa contina doar litere!");
          error.push("Prenumele trebuie sa contina doar litere!");
        }
        if (telefon.length != 10) {
          console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
          error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
        } else if (!telefon.match("^[0-9]+$")) {
          console.log("Numarul de telefon trebuie sa contina doar cifre!");
          error.push("Numarul de telefon trebuie sa contina doar cifre!");
        }
        if (!email.includes("@gmail.com") && !email.includes("@yahoo.com")) {
          console.log("Email invalid!");
          error.push("Email invalid!");
        }
        if(!facebook.includes("https://www.facebook.com/")){
        console.log("Link de Facebook invalid!");
        error.push("Link de Facebook invalid!");
        }
        if(tipAbonament!="Basic" || tipAbonament!="Standard" || tipAbonament!="Premium"){
        console.log("Tipul de abonament este invalid!");
        error.push("Tipul de abonament este invalid!");
        }

        if (nrCard.length != 16) { 
        console.log("Numarul de card trebuie sa fie de 16 cifre!");
        error.push("Numarul cardului trebuie sa fie de 16 cifre!");
        }else if (!nrCard.match("^[0-9]+$")){
            console.log("Numarul cardului trebuie sa contina doar cifre!");
            error.push("Numarul cardului trebuie sa contina doar cifre!");
        }
        if (cvv.length != 3) { 
            console.log("Cvv-ul trebuie sa fie de 3 cifre!");
            error.push("Cvv-ul trebuie sa fie de 3 cifre!");
            }else if (!cvv.match("^[0-9]+$")){
                console.log("Cvv-ul trebuie sa contina doar cifre!");
                error.push("Cvv-ul trebuie sa contina doar cifre!");
            }

        if(varsta.length < 1 || varsta.length > 3){
            console.log("Varsta invalida!");
            error.push("Varsta invalida!");
        }else if(!varsta.match("^[0-9]+$")){
            console.log("Varsta trebuie sa contina doar cifre!");
            error.push("Varsta trebuie sa contina doar cifre!");
        }
        if(cnp.length != 13){
            console.log("CNP-ul trebuie sa contina 13 cifre!");
            error.push("CNP-ul trebuie sa contina 13 cifre!");
        }else if(!cnp.match("^[0-9]+$")){
            console.log("CNP-ul trebuie sa contina doar cifre!");
            error.push("CNP-ul trebuie sa contina doar cifre!");
        }
        //presupunem ca in partea de front-end se specifica regula de introducere a datei de nastere AALLZZ
        //de asemenea presupunem ca toti oamenii care vor netflix sunt nascuti dupa 1919 :)
        if(dataNasterii != 6){
            console.log("Data de nastere invalida!");
            error.push("Data de nastere invalida!");
        }else if(!dataNasterii.match("^[0-9]+$")){
            console.log("Data nasterii trebuie sa contina doar cifre!");
            error.push("Data nasterii trebuie sa contina doar cifre!");
        }
        if(dataNasterii[0]!=cnp[1] || dataNasterii[1]!=cnp[2] || dataNasterii[2]!=cnp[3] || dataNasterii[4]!=cnp[5] || dataNasterii[5]!=cnp[6]){
            console.log("Data nasterii introdusa si CNP-ul nu sunt compatibile");
            error.push("Data nasterii introdusa si CNP-ul nu sunt compatibile");
        }
        if(cnp[0]===1 || cnp[0]===3 || cnp[0]===5)
            sex="M";
        else if(cnp[0]===2 || cnp[0]===4 || cnp[0]===6)
            sex="F";
        }

    if (error.length === 0) {
        const sql =
            "INSERT INTO abonament (nume,prenume,telefon,email,facebook,tipAbonament,nrCard,cvv,varsta,cnp,sex,dataNasterii) VALUES('" +nume +"','" +prenume +"','" +telefon +"','" +email +"','" +facebook +"','" + tipAbonament +"','" + nrCard +"','" +cvv+"','"+ varsta+"','"+cnp+"','"+sex+"','"+dataNasterii+"')";
        connection.query(sql, function (err, result) {
            if (err) throw err;
            console.log("Abonament achizitionat!");
        });

        res.status(200).send({
            message: "Abonament achizitionat!"
        });
        console.log(sql);
    } else {
        res.status(500).send(error)
        console.log("Eroare la inserarea in baza de date!");
    }
});